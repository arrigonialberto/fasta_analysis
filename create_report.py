#!/usr/bin/env python

import re

def write_html_report(fasta_name,summary_statistics,subs_stats,output_path):
	lengths_image_path = 'Lengths_distribution.png'
	percentage_id_path = 'Identity_percentage.png'
	f = open(summary_statistics,'r')
	summary_result = ''
	for line in f:
		e = line.strip().split(":")
		summary_result = summary_result+"<h4 style='text-align:center'>{0}:\t{1}</h4>".format(e[0],e[1])
	f.close()

	f = open(subs_stats,'r')
	modifications = {'del':[],'ins':[],'subs':[]}
	state = 0
	for line in f:
		if line.strip()=='--Deletion--': 
			continue
		elif line.strip()=='--Insertion--': 
			state = 1
			continue
		elif line.strip()=='--Substitution--':
			state = 2
			continue
		if state==0:
			modifications['del'].append(line.strip())
		elif state==1:
			modifications['ins'].append(line.strip())
		elif state==2:
			modifications['subs'].append(line.strip())

	nct = ['A','T','C','G']
	table_header = '<tr><th>Deletion</th><th>Insertion</th></tr>'
	del_ins_table = ''

	table_body = ''
	for i in range(0,len(modifications['del'])):
		table_body += "<tr><td>{0}</td> <td>{1}</td></tr>".format(modifications['del'][i],modifications['ins'][i])

	sub_table_header = '<tr><th>Base 1</th><th>Base 2</th><th>Frequency</th></tr>'
	sub_table_body = ''
	for i in modifications['subs']:
		sub_table_body += "<tr><td>{0}</td> <td>{1}</td> <td>{2}</td></tr>".format(i.split()[0],i.split()[2].replace(":",""),i.split()[3])


	header = """<head>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	</head>"""

	body = """<body>
	<nav class="navbar navbar-default" role="navigation">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="#">
	      
	      </a>
	    </div>
	  </div>
	</nav>
		<div class='container'>
			<h1 style='text-align:center'>Summary statistics for {0}</h1>
			<div class='row' style='background-color:#E8E8E8' ><div class='col-md-2'></div>
				<div class='.col-md-6'><img class="img-thumbnail" src="{1}" width=576px heigth=432px></div>
			</div>
			<div class='row'  style='background-color:#D0D0D0'><div class='col-md-2'></div>
				<img class="img-thumbnail" src="{2}" width=576px heigth=432px>
			</div>
			<div>{3}</div>
			<div class='row'><div class='col-md-3'></div>
			<div class='col-md-6'><table class="table table-striped" >
			{4}
			</table></div></div>
			<div class='row'><div class='col-md-3'></div>
			<div class='col-md-6'><table class="table table-striped" >
			{5}{6}
			</table></div></div>

		</div>
	</body>""".format(fasta_name, lengths_image_path, percentage_id_path, summary_result, table_header+table_body, sub_table_header, sub_table_body)

	with open(output_path, 'w') as f:
		for i in header: f.write(i)
		for i in body: f.write(i)






