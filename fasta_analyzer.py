#!/usr/bin/env python

import argparse
import re
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import gaussian_kde
from Bio import SeqIO
import os, re, sys
from Bio import Seq
from collections import defaultdict
import BioReaders
import SeqReaders
import create_report as Rep
from optparse import OptionParser

def gen_lengths_distribution(fasta_file):
	print "Generating read lengths distribution..."
	length_distribution = []
	handle_fasta = open(fasta_file,'rU')
	for seq_record in SeqIO.parse(handle_fasta, "fasta"):
		length_distribution.append(len(seq_record))
	handle_fasta.close()
	return length_distribution

def mean(l):
	return	float(sum(l))/len(l) if len(l) > 0 else float('nan')

def gen_lengths_distribution_plot(distribution,directory):
	density = gaussian_kde(distribution)
	xs = np.linspace(0,max(distribution),500)
	density.covariance_factor = lambda : .35
	density._compute_covariance()
	plt.title('Lengths distribution')
	plt.xlabel('Length (bp)')
	plt.ylabel('Density')
	plt.grid(True)
	plt.plot(xs,density(xs), linewidth=3)
	plt.savefig('./'+directory+'/Lengths_distribution.png')
	plt.close()

def gen_perc_identity_plot(distribution,directory):
	n, bins, patches = plt.hist(distribution, 50, normed=1, facecolor='g', alpha=0.75)
	plt.xlabel('Identity percentage')
	plt.ylabel('Numerosity')
	plt.title('Identity percentage (%) distribution')
	plt.grid(True)
        plt.savefig('./'+directory+'/Identity_percentage.png')
        plt.close()

def stat_main(fasta_input, sam_input, directory):
	print "Calculating statistics for {0} and {1}".format(fasta_input,sam_input)
	# Generate read lengths distribution
	distribution = gen_lengths_distribution(fasta_input)
		
	# Create output directory and read lengths plot
	if not os.path.exists(directory):
	    os.makedirs(directory)
	gen_lengths_distribution_plot(distribution,directory)

	# Write summary statistics for fasta file
	write_fasta_statistics(directory, distribution)

	#Generate .sam statistics
	identity_perc,deletions,insertions = gen_sam_statistics(sam_input)

	# Generate histograms % identity plot
	gen_perc_identity_plot(identity_perc, directory)

	#Write summary statistics for sam file
 	write_sam_statistics(directory, deletions, insertions)


def write_fasta_statistics(directory, distribution): 
	with open('./'+directory+'/Summary_statistics.txt', 'w') as f:
		f.write("Number of sequences: %d\n" % len(distribution))
		f.write("Min read length: %d\n" % min(distribution))
		f.write("Max read length: %d\n" % max(distribution))
		f.write("Mean read length: %d\n" % mean(distribution))

def gen_sam_statistics(sam_input):
	""" Calculate the average identity percentage and the average number of insertions and deletions for the input .sam file. """
	identity_tot = []
	deletions = []
	insertions = []
	reader = BioReaders.GMAPSAMReader(sam_input, True)
	for i in reader:
		if i.identity and i.num_del and i.num_ins:
			identity_tot.append(i.identity)
			deletions.append(i.num_del)
			insertions.append(i.num_ins)
        return identity_tot,deletions,insertions

def write_sam_statistics(directory, deletions, insertions): 
	with open('./'+directory+'/Summary_statistics.txt', 'a') as f:
		f.write("Average # of deletions: %d\n" % mean(deletions))
		f.write("Average # of insertions: %d\n" % mean(insertions))


cigar_rex = re.compile('(\d+)(\S)')
def unpack_cigar_string(cigar):
    """
    Unpack a cigar string (ex: 3M1I2D) 
    to a list (ex: ['M','M','M','I','D','D']        
    """
    result = []
    strlen = 0
    for m in cigar_rex.finditer(cigar):
        strlen += len(m.group(1)) + len(m.group(2))
        num, type = int(m.group(1)), m.group(2)
        result += [type] * num
    assert len(cigar) == strlen
    return result


def write_report(err, output_filename):
    nts = ['A','T','C','G']
    with open(output_filename, 'w') as f:
        f.write("--Deletion--\n")
        _sum = sum(err['D'].itervalues())
        for x in nts:
            f.write("{0} -> -: {1:.2f}\n".format(x, err['D'][('-',x)]*1./_sum))
        f.write("--Insertion--\n")
        _sum = sum(err['I'].itervalues())
        for x in nts:
            f.write("{0} -> -: {1:.2f}\n".format(x, err['I'][(x,'-')]*1./_sum))
        f.write("--Substitution--\n")
        _sum = sum(err['S'].itervalues())
        for x in nts:
            for y in nts:
                if x == y: continue
                f.write("{0} -> {1}: {2:.2f}\n".format(x, y, err['S'][(y,x)]*1./_sum))


def main_subs(reader, input, genome, err):
    """
    Calculate substitution rate for selected fasta
    """
    for r in reader:
        if r.sID != 'MT': r.sID = 'chr'+r.sID
        if r.sID not in genome:
            print "skipping", r.sID
            continue
        qstr=Seq.Seq(input[r.qID].sequence.upper())
        tstr=genome[r.sID].seq[r.sStart:r.sEnd].upper()
        if r.flag.strand=='-': qstr=qstr.reverse_complement()
	x=unpack_cigar_string(r.cigar)
	iter=iter_cigar(x, qstr, tstr)
        for a,b,c in iter: err[a][(b,c)]+=1

def iter_cigar(cigar_list, qstr, tstr):
    i = 0
    j = 0
    for x in cigar_list:
        if x == 'S':
            i += 1
        elif x == 'N':
            j += 1
        elif x == 'M':
            if qstr[i] != tstr[j]:
                yield 'S',qstr[i], tstr[j]
            i += 1
            j += 1
        elif x == 'D':
            yield 'D','-', tstr[j]
            j += 1
        elif x == 'I':
            yield 'I',qstr[i], '-'
            i += 1
        elif x == 'H' and i > 0:
            return
    print i, j

def splitCount(s, count):
    """ 
    Used to print formatted fasta sequences
    """
    return [s[i:i+count] for i in range(0, len(s), count)]

def split_fasta(fasta,chem_structure,dir_name):
    """
    Split input fasta according to the categories defined in the configuration file.
    """
    chems = chem_structure.keys()
    files = ["./{0}/".format(dir_name)+fasta.replace('.fa','')+"_"+str(chems[x])+".fa" for x in range(len(chems))]
    handles = [open("./{0}/".format(dir_name)+fasta.replace('.fa','')+"_"+str(chems[x])+".fa",'w+') for x in range(len(chems))]
    chem_dict = defaultdict(str)
    for i in chem_structure.keys():
             for k in chem_structure[i]: chem_dict[k]=i
    for seq_record in SeqIO.parse(fasta, "fasta"):
	if chem_dict[seq_record.description.split()[0].split("/")[0]]=='':
		print "Movie name not found in configuration file: {0}".format(seq_record.description.split()[0].split("/")[0])
		continue
        index =  chems.index(chem_dict[seq_record.description.split()[0].split("/")[0]])
    	stream = handles[index]
	seq = ''
	for i in splitCount(seq_record.seq,60): seq+=i+"\n"
	seq.strip()
	stream.write(">{0}\n{1}".format(seq_record.description,seq))			
    for s in handles: s.close()
    return files

def split_sam(sam,chem_structure,dir_name):
    """
    Split input sam according to the categories defined in the configuration file.
    """
    chems = chem_structure.keys()
    files = ["./{0}/".format(dir_name)+sam.replace('.sam','')+"_"+str(chems[x])+".sam" for x in range(len(chems))]
    handles = [open("./{0}/".format(dir_name)+sam.replace('.sam','')+"_"+str(chems[x])+".sam",'w+') for x in range(len(chems))]
    chem_dict = defaultdict(str)
    for i in chem_structure.keys():
        for k in chem_structure[i]: chem_dict[k]=i
    for record in open(sam,'r'):
	if record.startswith('@'):
	    for i in handles: i.write(record)
        elif chem_dict[record.split()[0].split("/")[0]]=='':
            print "Movie not listed in configuration file: {0}".format(record.split()[0].split("/")[0])
            continue
	else:
            index =  chems.index(chem_dict[record.split()[0].split("/")[0]])
            stream = handles[index]
            stream.write(record.strip()+"\n")
    for s in handles: s.close()
    return files


def main(fasta_input,sam_input,genome):
    directory =  os.path.splitext(fasta_input)[0]

    # Calculate reads quality and mapping statistics for .fasta and .sam input files    
    stat_main(fasta_input, sam_input, directory)
    reader=BioReaders.GMAPSAMReader(sam_input,True)
    err = {}
    err={'S': defaultdict(lambda:0), 'I': defaultdict(lambda: 0), 'D':defaultdict(lambda: 0)}
    input=SeqReaders.LazyFastaReader(fasta_input)
    
    # Generate and write a text report about base substitutions rate
    main_subs(reader, input, genome, err)
    write_report(err, './'+directory+'/Subs_stats.txt')
    print './'+directory+'/Subs_stats.txt'
    print 'Writing html reports...'
    Rep.write_html_report(fasta_input,'./'+directory+'/Summary_statistics.txt','./'+directory+'/Subs_stats.txt','./'+directory+'/Report.html')

def parse_config(file_name):
    diz = defaultdict(list)
    for line in open(file_name,'r'):
        if re.match('.*group.*', line): continue
        c = line.strip().replace(" ","").split(',')
        diz[c[0]].append(c[1])
    return diz
	
def directory_name_from_fasta(fasta_name):
    # assuming sample name is: sampleXX.(.*).fasta
    return fasta_name.split('.')[0]


if __name__ == "__main__":
    """
    This script generates a folder named after the basename of the input fasta (sample.fa -> sample)
    If no config_file is present, summary statistics files and an html report are included 
    in this folder.
    If a config_file is provided, a subfolder (./sample/chemistry_1_sample) is created for each 
    one of the defined categories that contains summary statistics files.   

    This script calculates and reports statistics for the input fasta/sam files.
        Summary_statistics.txt contains insertion, deletion and identity percentage averages
        Subs_stats.txt contains bases substitution rates for the selected sample 

    An html report is created that summarizes the statistics calculated for the selected sample. 
    Two plots are included in the report: 1) A read lengths density distribution 2) A bar plot
    that shows calculated identity percentages 

    Example usage:

    python fasta_analyzer.py <sample.fa> <sample.sam> --config_file <movies.txt> --genome <genome.fa path>

    """
    parser = argparse.ArgumentParser()
    parser.add_argument("fasta_name", help="Input fasta file name")
    parser.add_argument("sam_name", help="Input sam file name")
    parser.add_argument("--config_file", help="Input configuration file ex. sample_movie.txt")    
    parser.add_argument("--genome", help="Path to .fasta genome")
    args = parser.parse_args()
    fasta_input = args.fasta_name  
    sam_input = args.sam_name
    if args.genome: 
	genome=args.genome
    else:
        genome='/path_to_default_genome_fasta_location'
    if args.config_file:
        # If movies are categorized the script generates .fasta and .sam files splitted according to categorization
        try:
        	chem_structure = parse_config(args.config_file)
        	#print chem_structure
        except:
        	raise Exception('An error has occurred while parsing configuration file')
        dir_name = directory_name_from_fasta(fasta_input)
        if not os.path.exists(dir_name): os.mkdir(dir_name)

        print "Parsing sam file..."
        sam_list = split_sam(sam_input,chem_structure,dir_name)      
        print "Parsing fasta file..."
        fasta_list = split_fasta(fasta_input,chem_structure,dir_name)
        print 'Loading genome...'
        genome=SeqIO.to_dict(SeqIO.parse(open(genome),'fasta'))
        for i in range(len(fasta_list)):
            print fasta_list[i],sam_list[i]
            main(fasta_list[i],sam_list[i],genome)
    else:
        print 'Loading genome...'
        genome=SeqIO.to_dict(SeqIO.parse(open(genome),'fasta'))
        main(fasta_input,sam_input,genome)	


