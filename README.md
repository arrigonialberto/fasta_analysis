FASTA file analyzer
=====

This script generates a folder named after the basename of the input fasta (sample.fa -> sample)
If no config_file is present, summary statistics files and an html report are included 
in this folder.
If a config_file is provided, a subfolder (./sample/chemistry_1_sample) is created for each 
one of the defined categories that contains summary statistics files.   

This script calculates and reports statistics for the input fasta/sam files.
- Summary_statistics.txt contains insertion, deletion and identity percentage averages
- Subs_stats.txt contains bases substitution rates for the selected sample 

- An html report is created that summarizes the statistics calculated for the selected sample. Two plots are included in the report: 1) A read lengths density distribution 2) A bar plot that shows calculated identity percentages 

### Example usage:
```
    python fasta_analyzer.py <sample.fa> <sample.sam> --config_file <movies.txt> --genome <genome.fa path>
```
